<?php
/**
 * Contém uma classe helper para o menu principal do site
 *
 * PHP Version 5
 *
 * @author  André Terceiro <andreterceiro@yahoo.com.br>
 * @package View.Helper
 */

/**
 * Classe Helper para o menu principal do site
 *
 * @author  André Terceiro <andreterceiro@yahoo.com.br>
 * @package View.Helper
 */
class MenuHelper extends AppHelper
{
    /**
     * Opções a serem apresentadas no menu e suas configurações
     *
     * @var array
     */
    private $_itens = array(
        'Home' => array(
            'link' => '/',
            'selecionadaSePagina' => array('/', '/pages/index/')
        ),
        'Panatron' => array(
            'link' => '/quem-somos/',
            'selecionadaSePagina' => array('/quem-somos/', '/pages/quemSomos/')
        ),
        'Serviços' => array(
            'link' => '/servicos/',
            'selecionadaSePagina' => array('/servicos/', '/pages/servicos/')
        ),
        'Contato' => array(
            'link' => '/contato/',
            'selecionadaSePagina' => array('/contato/', '/pages/contato/')
        ),
    );

    /**
     * Retorna o HTML do menu
     *
     * @param  string $paginaSelecionada A página que deve ser mostrada como selecionada no menu
     *
     * @author André Terceiro <andre@promosapiens.com.br>
     * @access public
     * @return string
     */
    public function obter($paginaSelecionada='')
    {
        if (! is_string($paginaSelecionada)) {
            $paginaSelecionada = '';
        }

        $retorno = '<ul class="nav nav-pills" style="padding-left:78px">';
        foreach($this->_itens as $labelItemMenu => $configuracoesItemMenu) {
            if (in_array($paginaSelecionada, $configuracoesItemMenu['selecionadaSePagina'])) {
                $classeLiMenu = 'active';
            } else {
                $classeLiMenu = 'menu-opcao-nao-selecionada';
            }

            $retorno .= '<li class="' . $classeLiMenu . '">';
            $retorno .= '    <a href="' . $configuracoesItemMenu['link'] . '">';
            $retorno .= '        ' . $labelItemMenu;
            $retorno .= '     </a>';
            $retorno .= '</li>';
        }
        $retorno .= '</ul>';

        return $retorno;
    }
}

?>