<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $title_for_layout; ?>
    </title>
    <?php
        /* Twitter bootstrap (com Jquery)*/
        echo $this->Html->script('jquery.min.js');
        echo $this->Html->script('bootstrap.min.js');
        echo $this->Html->css('bootstrap.min.css');

        /* CSS padrao */
        echo $this->Html->css('geral.css?v=2');

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
</head>
<body>
    <div class="container">
        <div class="span12">
            <div class="row">
                <div class="span12" id="flash-messages">
                    <?php
                        // Flash messages
                        echo $this->Session->flash();
                    ?>
                </div>
            </div>

            <?php /* Testeira, com menu */ ?>
        	<div class="row">
                <div id="cabecalho" class="span12">
                    <div class="row">
                		<div id="container-logo" class="span5">
                            <?php
                                echo $this->Html->image(
                                    'logo.png',
                                    array(
                                        'alt' => 'Logo da Panatron',
                                        'url' => '/',
                                        'id'  => 'logo-cabecalho'
                                    )
                                );
                                ?>
                		</div>
                		<div id="container-menu" class="span6 offset1">
                			<?php
                				echo $this->Menu->obter($this->here);
            				?>
            			</div>
                    </div>
                </div>
    		</div>

            <?php /* Conteúdo em geral, a partir do endereço do topo */ ?>
    		<div class="row">
                <div class="span12" id="conteudo">
                    <div class="row">
            			<div class="span1" id="espacamento-esquerda-meio-conteudo"></div>
            			<div class="span10">
            				<?php /* Endereço do topo */ ?>
                            <div class="row">
            					<div class="span10" id="contato-header">
            						<?php echo $this->Contato->obterEnderecoCompleto();	?><br />
                                    Telefones: <?php echo $this->Contato->obterTelefones(); ?>
            					</div>
            				</div>
                            <div class="row">
                                <div class="span10" id="filete-abertura-conteudo"></div>
                            </div>


                            <?php
                                // Conteúdo da view
                                echo $this->fetch('content');
                            ?>

                            <?php /* Rodapé */ ?>
                            <div class="row">
                                <div class="span10" id="rodape">
                                    <?php echo $this->Contato->obterCompleto(); ?>
                                </div>
                            </div>
            			</div>
            			<div class="span1" id="espacamento-direita-meio-conteudo"></div>
                    </div>
                </div>
    		</div>
        </div>
    </div>
    <?php echo $this->Element('analytics'); ?>
</body>
</html>
