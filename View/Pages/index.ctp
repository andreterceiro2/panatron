<?php
    $this->Html->meta(
        'description',
        'Assistência técnica em telefonia e segurança - Panasonic e Intelbras. Rapidez, alta qualidade e garantia nos consertos. Tel.: ' . $this->Contato->telefone,
        array('type' => 'description', 'inline' => false)
    );

    echo $this->Html->css('pages/index', null, array('inline'=>false));
?>
<?php /* Banner principal */ ?>
<div class="row">
    <div class="span10" id="banner-principal">
        <div id="carrossel-banner-principal" class="carousel slide">
            <ol class="carousel-indicators">
                <li data-target="#carrossel-banner-principal" data-slide-to="0" class="active"></li>
                <li data-target="#carrossel-banner-principal" data-slide-to="1"></li>
            </ol>
            <!-- Carousel items -->
            <div class="carousel-inner">
                <div class="active item">
                    <?php
                        echo $this->Html->image(
                            'pages/index/banner-principal/telefonia.jpg',
                            array(
                                'alt' => 'Banner sobre assistência técnica em telefonia'
                            )
                        );
                    ?>
                    <div class="carousel-caption">
                        <h4>Assistência técnica em Telefonia</h4>
                        <p>
                            Fazemos manutenção em toda linha fabricada pela Panasonic: centrais telefônicas (PABX) <br />
                            analógicas e digitais, KS, telefones sem fio e com fio, aparelhos de fax, interfaces, entre outros.
                        </p>
                    </div>
                </div>
                <div class="item">
                    <?php
                        echo $this->Html->image(
                            'pages/index/banner-principal/seguranca.jpg',
                            array(
                                'alt' => 'Banner sobre assistência técnica em segurança'
                            )
                        );
                    ?>
                    <div class="carousel-caption">
                        <h4>Assistência técnica em Segurança</h4>
                        <p>
                            Realizamos manutenção em gravadores digitais de imagem da marca Intelbras. <br />
                            Temos treinamento feito com o fabricante.
                        </p>
                    </div>
                </div>
            </div>
            <!-- Carousel nav -->
            <a class="carousel-control left" href="#carrossel-banner-principal" data-slide="prev">&lsaquo;</a>
            <a class="carousel-control right" href="#carrossel-banner-principal" data-slide="next">&rsaquo;</a>
        </div>
    </div>
</div>

<?php /* Banners/links para outras áreas */ ?>
<div class="row">
    <div class="span10" id="links-areas">
        <div class="item-link-areas">
            <?php
                echo $this->Html->image(
                    'pages/index/links-areas/quem-somos.png',
                    array(
                        'alt' => 'Quem somos',
                        'url' => '/quem-somos/'
                    )
                );
            ?>
        </div>
        <div class="item-link-areas">
            <?php
                echo $this->Html->image(
                    'pages/index/links-areas/pabx.png',
                    array(
                        'alt' => 'PABX',
                        'url' => '/servicos/'
                    )
                );
            ?>
        </div>
        <div class="item-link-areas">
            <?php
                echo $this->Html->image(
                    'pages/index/links-areas/seguranca.png',
                    array(
                        'alt' => 'Segurança',
                        'url' => '/servicos/'
                    )
                );
            ?>
        </div>
        <div class="item-link-areas" id="item-link-areas-contato">
            <?php
                echo $this->Html->image(
                    'pages/index/links-areas/contato.png',
                    array(
                        'alt' => 'Contato',
                        'url' => '/contato/'
                    )
                );
            ?>
        </div>
    </div>
</div>

<?php /* Diferenciais da Panatron */ ?>
<div class="row">
    <div class="span10" id="diferenciais-panatron">
        <div class="coluna-diferenciais-panatron">
            <div class="item-diferencial-panatron">
                <div class="icone-diferencial-panatron">
                    <?php echo $this->Html->image('pages/index/icones-diferenciais-panatron/rapidez.png'); ?>
                </div>
                <div class="texto-diferencial-panatron">
                    <b>Rapidez e qualidade:</b> manutenção executada por pessoal técnico com larga experiência em assistência técnica na área de telefonia e segurança, proporcionando prazos curtos com padrão de qualidade bem acima da média do mercado.
                </div>
            </div>
            <div class="item-diferencial-panatron">
                <div class="icone-diferencial-panatron">
                    <?php echo $this->Html->image('pages/index/icones-diferenciais-panatron/pontualidade.png'); ?>
                </div>
                <div class="texto-diferencial-panatron">
                    <b>Pontualidade:</b> prazos para retiradas dos equipamentos mantidos com critério profissional.
                </div>
            </div>
            <div class="item-diferencial-panatron">
                <div class="icone-diferencial-panatron">
                    <?php echo $this->Html->image('pages/index/icones-diferenciais-panatron/pecas-originais.png'); ?>
                </div>
                <div class="texto-diferencial-panatron">
                    <b>Peças originais:</b> a qualidade da manutenção é sustentada pela qualidade dos componentes, sempre originais.
                </div>
            </div>
        </div>
        <div class="coluna-diferenciais-panatron">
            <div class="item-diferencial-panatron">
                <div class="icone-diferencial-panatron">
                    <?php echo $this->Html->image('pages/index/icones-diferenciais-panatron/garantia.png'); ?>
                </div>
                <div class="texto-diferencial-panatron">
                    <b>Garantia:</b> A segurança do cliente está expressa na garantia fornecida pela assistência técnica. Nossos prazos vão de 90 dias a um ano de garantia, isso expresso no momento do fornecimento do orçamento.
                </div>
            </div>

            <div class="item-diferencial-panatron">
                <div class="icone-diferencial-panatron">
                    <?php echo $this->Html->image('pages/index/icones-diferenciais-panatron/contato-direto.png'); ?>
                </div>
                <div class="texto-diferencial-panatron">
                    <b>Contato direto -  retorno zero:</b> aqui os defeitos apresentados são passados diretamento ao pessoal técnico. Isso permite que a manutenção seja executada com precisão absoluta, evitando assim, os desagradáveis retornos, principalmente os que se originam por informações,  que não chegam ao departamento técnico.
                </div>
            </div>
        </div>
    </div>
</div>

