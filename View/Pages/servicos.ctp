<?php
    echo $this->Html->css('pages/servicos');
?>

<div class="row">
    <div class="span10 container-conteudo-geral">
        <div id="container-conteudo-bg-branco">
            <div class="container-servicos">

                <div class="container-carrossel">
                    <div id="carrossel-servicos" class="carrossel carousel slide">
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <?php
                                $fotos = array(
                                    array('imagem' => 'tda150.jpg', 'alt' => 'TDA 150'),
                                    array('imagem' => 'dvr-intelbras.jpg', 'alt' => 'DVR Intelbras'),
                                    array('imagem' => 'KS1.jpg', 'alt' => 'KS Panasonic'),
                                    array('imagem' => 'dvr-intelbras-2.jpg', 'alt' => 'DVR Intelbras'),
                                    array('imagem' => 'ns1000.jpg', 'alt' => 'NS 1000'),
                                    array('imagem' => 'NT346.jpg', 'alt' => 'NT 346'),
                                    array('imagem' => 'tda30.jpg', 'alt' => 'TDA 30'),
                                    array('imagem' => 'TES32.jpg', 'alt' => 'TES 32'),
                                );

                                $active = true;
                                foreach ($fotos as $foto) {
                            ?>
                                    <div class="item<?php echo $active ? ' active' : '';?>">
                                        <?php
                                            echo $this->Html->image(
                                                '/img/pages/servicos/' . $foto['imagem'],
                                                array(
                                                    'alt' => $foto['alt']
                                                )
                                            );
                                        ?>
                                    </div>
                            <?php
                                    $active = false;
                                }
                            ?>

                        </div>
                        <!-- Carousel nav -->
                        <a class="carousel-control left" href="#carrossel-servicos" data-slide="prev">&lsaquo;</a>
                        <a class="carousel-control right" href="#carrossel-servicos" data-slide="next">&rsaquo;</a>
                    </div>
                </div>

                <div class="container-dados-servico">
                    <div>
                        <h3>Telefonia / PABX</h3>
                    </div>
                    <div class="dados-servico">
                        Consertamos toda linha fabricada pela Panasonic: centrais telefônicas analógicas e digitais, KS, telefones sem fio e com fio, aparelhos de fax, interfaces etc.
                    </div>
                </div>
                <div class="container-dados-servico">
                    <div>
                        <h3>Segurança</h3>
                    </div>
                    <div class="dados-servico">
                        Consertamos gravadores digitais de imagem do fabricante Intelbras. Temos treinamento com o fabricante.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>