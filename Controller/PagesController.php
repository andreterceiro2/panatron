<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Pages';

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Configurações de títulos e opções do menu a serem selecionadas
 *
 * @var array
 */
	private $_configuracoes = array(
		'index' => array(
			'titulo' => 'Assistência técnica – Panasonic e Intelbras – Manutenção',
		),
	);

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			$this->redirect('/');
		}
		$page = $subpage  = null;
		$title_for_layout = $this->_obterTitulo($path[0]);

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}

		$this->set(compact('page', 'subpage', 'title_for_layout'));
		$this->render(implode('/', $path));
	}

	public function pingdom()
	{
		$this->autoRender = false;
		$this->layout = null;
		echo 'up';
		return;
	}

	/**
	 * Retorna o título da página
	 *
	 * @param string $pagina Página cujo título deve ser obtido
	 *
	 * @author André Terceiro <andreterceiro@yahoo.com.br>
	 * @access public
	 * @return string
	 */
	private function _obterTitulo($pagina)
	{
		$titulo = '';
		if (is_string($pagina)) {
			if (isset($this->_configuracoes[$pagina]['titulo'])) {
				$titulo = $this->_configuracoes[$pagina]['titulo'] .  ' | ' . 'Panatron';
			} else {
				$titulo = 'Panatron';
			}
		}

		return $titulo;
	}
}
