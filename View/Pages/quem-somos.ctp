<?php
    echo $this->Html->css('pages/quem-somos', null, array('inline' => false));
?>

<div class="row">
    <div class="span10 container-conteudo-geral">
        <div id="container-conteudo-bg-branco">
            <div class="imagens-quem-somos">
                <div id="container-imagem-construcao">
                    <?php echo $this->Html->image('pages/quem-somos/construcao.jpg'); ?>
                </div>
                <div id="container-selo-35-anos">
                    <?php echo $this->Html->image('pages/quem-somos/selo-35-anos.png'); ?>
                </div>
            </div>
            <div class="texto-quem-somos">
                <p>
                    Temos 35 anos de experiência na área de assistência técnica em aparelhos eletrônicos, sendo 15 anos na área de telecomunicações em toda linha de aparelhos Panasonic.
                </p>
                <p>
                    Há cinco anos também trabalhamos com gravadores digitais de imagem (DVR) do fabricante Intelbras, com treinamento feito com o fabricante.
                </p>
                <p>
                    Todos consertos são efetuados com peças originais e possuem garantia de até 1 ano, informada no orçamento. Temos total consciência da importância dos equipamentos para os clientes, por isto executamos os consertos com rapidez e permitimos que o cliente explique os problemas diretamente para a equipe técnica, evitanto problemas de defeitos intermitentes não encontrados.
                </p>
            </div>
        </div>
    </div>
</div>
