<?php
    echo $this->Html->css('pages/contato');
?>

<div class="row">
    <div class="span10 container-conteudo-geral">
        <div id="container-conteudo-bg-branco">
            <div id="container-imagem-contato">
                <?php
                    echo $this->Html->image(
                        '/img/pages/contato/contato.png',
                        array(
                            'alt' => 'Contato'
                        )
                    );
                ?>
            </div>
            <div id="container-dados-contato">
                <div id="titulo-dados-contato">
                    <h3>Fale conosco</h3>
                </div>
                <div id="dados-contato">
                    <ul>
                        <li>
                            <b>Telefone: </b>
                            <?php echo $this->Contato->telefone;?>
                        </li>
                        <li>
                            <b>Celular (Claro): </b>
                            <?php echo $this->Contato->celularOi;?>
                        </li>
                        <li>
                            <b>E-mail: </b>
                            <a href="mailto:<?php echo $this->Contato->email;?>">
                                <?php echo $this->Contato->email;?>
                            </a>
                        </li>
                        <li>
                            <b>Enedereço: </b>
                            <?php echo $this->Contato->obterEnderecoSemBairro(); ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
