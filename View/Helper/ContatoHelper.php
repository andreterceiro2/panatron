<?php
/**
 * Contém uma classe helper para os dados de contato
 *
 * PHP Version 5
 *
 * @author  André Terceiro <andreterceiro@yahoo.com.br>
 * @package View.Helper
 */

/**
 * Classe Helper para os dados de contato
 *
 * Os dados de contato não foram mantidos em um Model porque só são usados para apresentação
 *
 * @author  André Terceiro <andreterceiro@yahoo.com.br>
 * @package View.Helper
 */
class ContatoHelper extends AppHelper
{
    /**
     * Opções a serem apresentadas no menu e suas configurações
     *
     * @var array
     */
    private $_dados = array(
        'telefone'   => '(11) 2021-4279',
        'endereco'   => 'Rua São Marinho, 25A',
        'bairro'     => 'Vila Formosa',
        'cidade'     => 'São Paulo',
        'estado'     => 'SP',
        'email'      => 'contato@panatron.com.br',
        'celularOi'  => '(11) 98066-2262',
        'celularTim' => '',
    );

    /**
     * Retorna o valor correspondente ao índice em $this->_dados, se existir
     *
     * @param  string $item Propriedade pública inexistente solicitada
     *
     * @author André Terceiro <andreterceiro@yahoo.com.br>
     * @access public
     * @return string
     */
    public function __get($item)
    {
        if (isset($this->_dados[$item])) {
            return $this->_dados[$item];
        }

        return '';
    }

    /**
     * Retorna uma linha com o endereço completo e os celulares
     *
     * @author André Terceiro <andreterceiro@yahoo.com.br>
     * @access public
     * @return string
     */
    public function obterCompleto()
    {
        return $this->obterTelefones() .
               '&nbsp; &#8226; &nbsp;' .
               $this->obterEnderecoCompleto();
    }

    /**
     * Retorna uma linha com todos telefones
     *
     * @author André Terceiro <andreterceiro@yahoo.com.br>
     * @access public
     * @return string
     */
    public function obterTelefones()
    {
        return $this->_dados['telefone'] .
               '&nbsp; &#8226; &nbsp;' .
               $this->_dados['celularOi'];
   }

    /**
     * Retorna uma linha com o endereço completo
     *
     * @author André Terceiro <andreterceiro@yahoo.com.br>
     * @access public
     * @return string
     */
    public function obterEnderecoCompleto()
    {
        return $this->_dados['endereco'] .
               '&nbsp; &#8226; &nbsp;' .
               $this->_dados['bairro'] .
               '&nbsp; &#8226; &nbsp;' .
               $this->_dados['cidade'] .
               '&nbsp; &#8226; &nbsp;' .
               $this->_dados['estado'];
    }

    /**
     * Retorna todas informações de endereço, exceto o bairro
     *
     * @author André Terceiro <andreterceiro@yahoo.com.br>
     * @access public
     * @return string
     */
    public function obterEnderecoSemBairro()
    {
        return $this->_dados['endereco'] .
               '&nbsp; &#8226; &nbsp;' .
               $this->_dados['cidade'] .
               '&nbsp; &#8226; &nbsp;' .
               $this->_dados['estado'];
    }

}
?>
