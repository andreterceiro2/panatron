<?php
    echo $this->Html->css('pages/materiais-tecnicos');
?>

<div class="row">
    <div class="span10 container-conteudo-geral">
        <div id="container-conteudo-bg-branco">
            <div id="titulo-materiais-tecnicos">
                <h3>Materiais técnicos</h3>
            </div>
            <div id="container-tabela-materiais-tecnicos">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Empresa</th>
                            <th>Download</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>TD500 - manual</th>
                            <th>Panasonic</th>
                            <th><a href="">clique aqui</a></th>
                        </tr>
                        <tr>
                            <th>TD500 - atualização de firmware v.012</th>
                            <th>Panasonic</th>
                            <th><a href="">clique aqui</a></th>
                        </tr>
                        <tr>
                            <th>TX1232 - manual</th>
                            <th>Panasonic</th>
                            <th><a href="">clique aqui</a></th>
                        </tr>
                        <tr>
                            <th>KS XYZ - manual</th>
                            <th>Panasonic</th>
                            <th><a href="">clique aqui</a></th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>